###
#Script for automated Jenkins deploy process
###

if [ $( docker ps -aq -f name=thumbgen-deploy-container ) ]; then
  echo "Container exists. Remove it"
  docker rm -f thumbgen-deploy-container
else
  echo "No container to remove"
fi