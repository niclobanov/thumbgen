###
#Script for automated Jenkins build process
###

if [ $( docker ps -aq -f name=thumbgen-build-container ) ]; then
  echo "Container exists. Remove it"
  docker rm -f thumbgen-build-container
else
  echo "No container to remove"
fi