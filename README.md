**thumbgen - приложение для генерации миниатюр изображений по средствам REST API**
------------------

Данное REST API приложение для генерации миниатюр изображений построено на основе программной платформы Java 8 и фрэймворка Spring,  
в частности его реализации convention-over-configuration Spring Boot 2. Для обеспечения основного функционала приложения используется  
стандартные плагины от сообществ Apache, Maven и Spring соответственно. Для генерации миниатюр используется open source библиотека  
thumbnailator от `http://coobird.net/`. Реализовно обычное итеграционное и модульное тестирование стандартного функционала.  
(без failover тестирования и прочего).

*Для запуска и работы приложений вам потребуются следующие свободные порты: 8080 (для Jenkinks CI),  
8081 (для запуска thumbgen вручную) и 8091 (для запуска thumbgen с помощью Jenkinks CI).*

*Проект можно импортировать в IDE Eclipse, сборку STS-3.9.6 (Spring Tools Suite).* 

*Рекомендуемая среда для развертывания CI - Debian GNU/Linux 9 (stretch) или Ubuntu 16.04 LTS (Xenial).*

*Для выполнения тестовых запросов потребуется наличие cURL* 

---

## Описание методов и форматов данных

Для обработки всех запросов в приложении используется один метод:

| Маршрут        | Тип запроса | Описание                                                                                                                 |
| ---------------|-------------|--------------------------------------------------------------------------------------------------------------------------|
| /image/process | POST        | Получает файлы с изображениями (multipart/form-data), JSON данные с base64 изображениями или ссылки на изображения в web |

**Формат принимаемых данных (см. далее примеры запросов):**
 
 - один или несколько фалов отправленных в качестве file параметра
 
 - массив в JSON из значений fileName и соответсвтующих им base64 encoded изображений:

  `[ { "fileName": "<назавание_изображения>" , "value" : "<base64_изображение>" }, { "fileName": "<назавание_изображения>" , "value" : "<base64_изображение>" }, ... ]`
 
 - одна или несколько ссылок отправленных в качестве url параметра

**Примеры запросов**

*1. Отправка файлов:*

  `curl -i -F file=@test-images/image_1.jpg http://localhost:8081/image/process`
  `curl -i -F file=@test-images/image_1.jpg -F file=@test-images/image_2.jpg http://localhost:8081/image/process`

*2. Отправка JSON данных с base64 encoded изображениями:*

  `curl -H "Content-Type: application/json" -d '[ { "fileName": "image_1.jpg" , "value" : "'"$( base64 -w 0 test-images/image_1.jpg )"'" } ]' http://localhost:8081/image/process`
  `curl -H "Content-Type: application/json" -d '[ { "fileName": "image_1.jpg" , "value" : "'"$( base64 -w 0 test-images/image_1.jpg )"'" } , { "fileName": "image_2.jpg" , "value" : "'"$( base64 -w 0 test-images/image_2.jpg )"'" } ]' http://localhost:8081/image/process`

*3. Отправка ссылок на изображения:*

  `curl -X POST -F 'url=https://bitbucket.org/niclobanov/thumbgen/raw/00d6ec570338c98b27fb4f1e85bf077440189ea6/test-images/image_1.jpg' http://localhost:8081/image/process`
  `curl -X POST -F 'url=https://bitbucket.org/niclobanov/thumbgen/raw/00d6ec570338c98b27fb4f1e85bf077440189ea6/test-images/image_1.jpg' -F 'url=https://bitbucket.org/niclobanov/thumbgen/raw/00d6ec570338c98b27fb4f1e85bf077440189ea6/test-images/image_2.jpg' http://localhost:8081/image/process`
 
**Формат возвращаемых данных - массив в JSON, содержащий имя файла, url или значение fileNane:**

  `[ { "item": "<идинтетификатор_изображения>" , "value" : "<base64_миниатюра_изображения>", "success" : "<true_или_false_в зависимости_от_результата_преобразования>" }, ... ]`

---

## Сборка и запуск средствами maven, jdk и jre

Для сборки и запуска приложения вручную потребуется **Open JDK** или **Oracle JDK** версии **1.8.0_181** и **maven версии 3.5.3.**. 

*1. Клонируем проект в вашу рабочую папку и переходим в него:*

  `cd ~`
  
  `git clone https://niclobanov@bitbucket.org/niclobanov/thumbgen.git`
  
  `cd ~/thumbgen`

*2. Выполняем сборку:*

 `mvn clean package`

*3. Запускаем  проект:*
  
 `java -jar ~/thumbgen/target/thumbgen-0.0.1-SNAPSHOT.jar`

*4. Проверяем работу приложения одним из тестовых запросов, указанных ранее.*

---

## Сборка и запуск с помощью docker-compose up

Для запуска  docker-compose up необходимы установленные **docker версии 18.09.0** и **docker-compose версии 1.22.0**.
Не забудьте добавить пользователя в группу docker (`usermod -a -G docker <ваща_учетная_запись>`)

*1. Клонируем проект в вашу рабочую папку и переходим в него:*

  `cd ~`

  `git clone https://niclobanov@bitbucket.org/niclobanov/thumbgen.git`
  
  `cd ~/thumbgen`

*2. Запускаем приложение с помощью docker-compose up:*

  `docker-compose up`

*3. Проверяем работу приложения одним из тестовых запросов, указанных ранее.*

---

## Организация CI на базе Jenkins

Для реализации CI/CD используется сервер автоматизации сборок Jenkins. С помощью сервера обеспечивается автоматизированная сборка имиджей 
и запуск контейнеров с приложением.

Для работы Jenkins потребуется **Open JDK** или **Oracle JDK** версии **1.8.0_181**.

**Инструкция по запуску и установке сервера автоматизированной сборки:**

*1. Загрузите Jenkins в вашу рабочую папку:*
  
  `cd ~`

  `curl -O http://ftp-nyc.osuosl.org/pub/jenkins/war-stable/2.138.3/jenkins.war`

*2. Запустите Jenkins:*

  `java -jar jenkins.war`

*3. Установите версию по умолчанию с набором стандартных плагинов. Используйте дефалтный password для входа. Создайте своего пользователя:*

![Дефалтный password](https://bitbucket.org/niclobanov/thumbgen/raw/4887d94c56b7b9dbfdfca0a7849cec766e7ba7cc/md-jenkins/jenkins_setup_1.png)

Дефалтный password

![Дефалтный password](https://bitbucket.org/niclobanov/thumbgen/raw/4887d94c56b7b9dbfdfca0a7849cec766e7ba7cc/md-jenkins/jenkins_setup_2.png)

Дефалтный password

![Установка стандартных плагинов](https://bitbucket.org/niclobanov/thumbgen/raw/4887d94c56b7b9dbfdfca0a7849cec766e7ba7cc/md-jenkins/jenkins_setup_3.png)

Установка стандартных плагинов

![Создание пользователя](https://bitbucket.org/niclobanov/thumbgen/raw/4887d94c56b7b9dbfdfca0a7849cec766e7ba7cc/md-jenkins/jenkins_setup_4.png)

Создание пользователя

*4. Для удобства просмотра результатов тестирования установите плагин Tests Result Analyzer:*

(см. https://wiki.jenkins.io/display/JENKINS/Test+Results+Analyzer+Plugin)

![Установка дополнительного плагина](https://bitbucket.org/niclobanov/thumbgen/raw/4887d94c56b7b9dbfdfca0a7849cec766e7ba7cc/md-jenkins/jenkins_setup_5.png)


**Инструкция по насторйке и запуска процесса CI:**

CI реализована на базе Jenkins Pipelines (бывший workflow). Для этого необходимо создать новый проект типа Pipeline.  
Для достаточно указать SCM проекта. Jenkins-файл подгрузится автоматически. Для запуска процесса сборки нажимаем build.  
При успешном выполнении всех шагов сборки и проверки результатов тестирования CI версия приложения будет доступна по адресу localhost:8091 .

В Jenkins возможно установить сборку по таймауту в формате cron или по наличию изменений в репозитарии (см. Build Triggers).  
Данный пример мы будем запускать вручную за отсутствием необходимости проведения постоянных проверок изменений, тестирования и сборок.  
Также возможно настроить автоматизированную сборку для ветвей (branches).

**Инструкция по запуску процесса CI для приложения thumbgen**

*1. Создаем новый проект типа pipeline*

![Создание нового проекта](https://bitbucket.org/niclobanov/thumbgen/raw/4887d94c56b7b9dbfdfca0a7849cec766e7ba7cc/md-jenkins/jenkins_ci_1.png)

*2. В настройках pipeline указываем определение "Pipeline script from SCM", тип SCM "Git" и 
копируем `https://niclobanov@bitbucket.org/niclobanov/thumbgen.git` в поле Repository URL:*

![Настройка pipeline](https://bitbucket.org/niclobanov/thumbgen/raw/4887d94c56b7b9dbfdfca0a7849cec766e7ba7cc/md-jenkins/jenkins_ci_2.png)

*3. Сохраняем проект и запускаем сборку (нажимаем **Build Now** с левой стороны экрана)*

*4. Проверяем результаты. Если все хорошо, то получается следующее:*

![Настройка pipeline](https://bitbucket.org/niclobanov/thumbgen/raw/aa2769c42c428ebbc32c431d640905fdfa67ca0f/md-jenkins/jenkins_ci_3.png)

*5. Проверяем работу приложения **тестовыми запросами указанными ранее используя порт 8091.***
