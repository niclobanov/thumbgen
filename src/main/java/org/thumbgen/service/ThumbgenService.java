package org.thumbgen.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Base64;

import org.springframework.stereotype.Service;

import net.coobird.thumbnailator.Thumbnails;

@Service
public class ThumbgenService {

	public static final int THUMBNAIL_WIDTH = 100;
	public static final int THUMBNAIL_HEIGHT = 100;
	
    public String generateThubnail(String imageBase64) throws IOException {
        byte[] decodedBytes = Base64.getDecoder().decode(imageBase64);
        ByteArrayInputStream bis = new ByteArrayInputStream(decodedBytes);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        Thumbnails.of(bis).size(THUMBNAIL_WIDTH, THUMBNAIL_HEIGHT).toOutputStream(bos);
        String retult = Base64.getEncoder().encodeToString(bos.toByteArray());
        return retult;
    }
    
    public String generateThubnail(InputStream fileStream) throws IOException {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        Thumbnails.of(fileStream).size(THUMBNAIL_WIDTH, THUMBNAIL_HEIGHT).toOutputStream(bos);
        String retult = Base64.getEncoder().encodeToString(bos.toByteArray());
        return retult;
    }
}
