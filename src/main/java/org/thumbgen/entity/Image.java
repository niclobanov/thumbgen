package org.thumbgen.entity;

public class Image {

    private String fileName;
    private String value;
    
    public Image() {}
    
    public Image(String fileName, String value) {
        super();
        this.fileName = fileName;
        this.value = value;
    }
    
    public String getFileName() {
        return fileName;
    }
    
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
    
    public String getValue() {
        return value;
    }
    
    public void setValue(String value) {
        this.value = value;
    }
}
