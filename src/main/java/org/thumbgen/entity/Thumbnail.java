package org.thumbgen.entity;

public class Thumbnail {

    private String item;
    private String value;
    private boolean success;
    
    public Thumbnail() {}
    
    public Thumbnail(String item, String value, boolean success) {
        super();
        this.item = item;
        this.value = value;
        this.success = success;
    }
    
    public String getItem() {
        return item;
    }
    
    public void setItem(String item) {
        this.item = item;
    }
    
    public String getValue() {
        return value;
    }
    
    public void setValue(String value) {
        this.value = value;
    }
    
    public boolean isSuccess() {
        return success;
    }
    
    public void setSuccess(boolean success) {
        this.success = success;
    }
    
    @Override
    public String toString() {
        return String.format("item: %s, success: %s, thumbnail: %s", 
                item, success, value);
    }
}
