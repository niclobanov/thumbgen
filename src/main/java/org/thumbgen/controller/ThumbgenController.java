package org.thumbgen.controller;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.thumbgen.entity.Image;
import org.thumbgen.entity.Thumbnail;
import org.thumbgen.service.ThumbgenService;
import org.thumbgen.utility.HttpDownloadUtility;

@RestController
public class ThumbgenController {
    
    private static final Logger logger = LoggerFactory.getLogger(ThumbgenController.class);
    
    @Autowired
    ThumbgenService thumbgenService;
    
    /**
     * Process images from specified urls
     * @param urls
     * @return json
     */
    @RequestMapping(value = "/image/process", params = "url", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<List<Thumbnail>> processUrlData(@RequestParam(name="url", required=false) String[] urls) {
        ArrayList<Thumbnail> thumbnails = new ArrayList<>();
        for(String url : urls) {
            Thumbnail thumb = null;
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            try {
                HttpDownloadUtility.downloadFile(url, outputStream);
                byte[] image = outputStream.toByteArray();
                ByteArrayInputStream bis = new ByteArrayInputStream(image);
                String value = thumbgenService.generateThubnail(bis);
                thumb = new Thumbnail(url, value, true);
            } catch (IOException e) {
                thumb = new Thumbnail(url, null, false);
                logger.error("image url {} processing error: {}", url, e.getMessage());
            }
            logger.info("thumbnail generation result: {}", thumb);
            thumbnails.add(thumb);
        }
        return new ResponseEntity<List<Thumbnail>>(thumbnails, HttpStatus.OK);
    }
    
    /**
     * Process image files
     * @param files
     * @return json
     */
    @RequestMapping(value = "/image/process", method = RequestMethod.POST, headers = "content-type=multipart/form-data")
    @ResponseBody
    public ResponseEntity<List<Thumbnail>> processMultipartFormData(@RequestParam(name="file", required=false) MultipartFile[] files) {
        ArrayList<Thumbnail> thumbnails = new ArrayList<>();
        for(MultipartFile file : files) {
            Thumbnail thumb = null;
            String item = file.getOriginalFilename();
            try {
                String value = thumbgenService.generateThubnail(file.getInputStream());
                thumb = new Thumbnail(item, value, true);
            } catch (IOException e) {
                thumb = new Thumbnail(item, null, false);
                logger.error("file item {} processing error: {}", item, e.getMessage());
            }
            logger.info("thumbnail generation result: {}", thumb);
            thumbnails.add(thumb);
        }
        return new ResponseEntity<List<Thumbnail>>(thumbnails, HttpStatus.OK);
    }
    
    /**
     * Process json with images in base64 format
     * @param images
     * @param request
     * @return json
     */
    @RequestMapping(value = "/image/process", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<List<Thumbnail>> processBase64Data(@RequestBody(required=false) Image[] images, HttpServletRequest request) {
        ArrayList<Thumbnail> thumbnails = new ArrayList<>();
        for (Image image : images) {
            Thumbnail thumb = null;
            String item = image.getFileName();
            try {
                String value = thumbgenService.generateThubnail(image.getValue());
                thumb = new Thumbnail(item, value, true);
            } catch (IOException e) {
                thumb = new Thumbnail(item, null, false);
                logger.error("base64 image {} processing error: {}", item, e.getMessage());
            }
            logger.info("thumbnail generation result: {}", thumb);
            thumbnails.add(thumb);
        }
        return new ResponseEntity<List<Thumbnail>>(thumbnails, HttpStatus.OK);
    }
}
