package org.thumbgen.utility;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Base64;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UtilBase64Image {
    
    private static final Logger logger = LoggerFactory.getLogger(UtilBase64Image.class);
    
    private UtilBase64Image() {}
    
    public static String encoder(String imagePath) {
        File file = new File(imagePath);
        try (FileInputStream imageInFile = new FileInputStream(file)) {
            String base64Image = null;
            byte imageData[] = new byte[(int) file.length()];
            imageInFile.read(imageData);
            base64Image = Base64.getEncoder().encodeToString(imageData);
            return base64Image;
        } catch (FileNotFoundException e) {
            logger.error("Image not found: {}", e);
        } catch (IOException ioe) {
            logger.error("Exception while reading the image: {}", ioe);
        }
        return null;
    }
    
    public static void decoder(String base64Image, String pathFile) {
        try (FileOutputStream imageOutFile = new FileOutputStream(pathFile)) {
            byte[] imageByteArray = Base64.getDecoder().decode(base64Image);
            imageOutFile.write(imageByteArray);
        } catch (FileNotFoundException e) {
            logger.error("Image not found: {}", e);
        } catch (IOException ioe) {
            logger.error("Exception while reading the image: {}", ioe);
        }
    }
}
