package org.thumbgen.utility;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
 
public class HttpDownloadUtility {
    
    public static final int BUFFER_SIZE = 4096;
    
    private static final Logger logger = LoggerFactory.getLogger(HttpDownloadUtility.class);
    
    private HttpDownloadUtility() {}
    
    /**
     * Downloads a file from a URL
     * @param fileURL HTTP URL of the file to be downloaded
     * @param ByteArrayOutputStream to store bytes
     * @throws IOException
     */
    public static boolean downloadFile(String fileURL, ByteArrayOutputStream outputStream)
            throws IOException {
        boolean result = false;
        URL url = new URL(fileURL);
        HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
        try {
            TrustModifier.relaxHostChecking(httpConn);
        } catch (KeyManagementException | NoSuchAlgorithmException | KeyStoreException e) {
            logger.warn("Relax host checking failure: {}", e);
        }
        int responseCode = httpConn.getResponseCode();
        
        if (responseCode == HttpURLConnection.HTTP_OK) {
            String fileName = "";
            String disposition = httpConn.getHeaderField("Content-Disposition");
            String contentType = httpConn.getContentType();
            int contentLength = httpConn.getContentLength();
            
            if (disposition != null) {
                int index = disposition.indexOf("filename=");
                if (index > 0) {
                    fileName = disposition.substring(index + 10,
                            disposition.length() - 1);
                }
            } else {
                fileName = fileURL.substring(fileURL.lastIndexOf("/") + 1,
                        fileURL.length());
            }
            
            logger.trace("Content-Type = {}", contentType);
            logger.trace("Content-Disposition = {}", disposition);
            logger.trace("Content-Length = {}", contentLength);
            logger.trace("fileName = {}", fileName);
            
            InputStream inputStream = httpConn.getInputStream();
            
            int bytesRead = -1;
            byte[] buffer = new byte[BUFFER_SIZE];
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, bytesRead);
            }
            
            outputStream.close();
            inputStream.close();
            
            logger.info("File downloaded: {}", fileURL);
            result = true;
        } else {
            logger.info("No file to download at {} - Server replied HTTP code: {}", fileURL, responseCode);
        }
        httpConn.disconnect();
        return result;
    }
}