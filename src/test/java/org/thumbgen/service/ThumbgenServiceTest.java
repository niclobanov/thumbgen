package org.thumbgen.service;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.thumbgen.service.ThumbgenService;

@RunWith(SpringRunner.class)
public class ThumbgenServiceTest {

    public static final String IMAGE_THUMBNAIL_CONTENT = "base64_thumbnail_content_here";
    
    @MockBean
    private ThumbgenService thumbgenService;
    
    @Test
    public void testGenerateThubnailString() throws IOException {
        when(thumbgenService.generateThubnail(any(String.class)))
            .thenReturn(IMAGE_THUMBNAIL_CONTENT);
        
        assertTrue(thumbgenService.generateThubnail("any string").equals(IMAGE_THUMBNAIL_CONTENT));
    }

    @Test
    public void testGenerateThubnailInputStream() throws IOException {
        when(thumbgenService.generateThubnail(any(InputStream.class)))
            .thenReturn(IMAGE_THUMBNAIL_CONTENT);
        
        assertTrue(thumbgenService.generateThubnail(new ByteArrayInputStream(new byte[] {})).equals(IMAGE_THUMBNAIL_CONTENT));
    }

}
