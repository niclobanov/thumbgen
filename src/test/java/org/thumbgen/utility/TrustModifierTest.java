package org.thumbgen.utility;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.thumbgen.utility.TrustModifier;

public class TrustModifierTest {

    private static final Logger logger = LoggerFactory.getLogger(TrustModifierTest.class);
    
    public static final String TEST_URL = "https://anyhttpsresource.org";
    
    @Test
    public void testRelaxHostChecking() throws MalformedURLException, KeyManagementException, NoSuchAlgorithmException, KeyStoreException {
        URL url = new URL(TEST_URL);
        HttpsURLConnection httpConn = null;
        try {
            httpConn = (HttpsURLConnection) url.openConnection();
            TrustModifier.relaxHostChecking(httpConn);
            assertTrue(httpConn.getHostnameVerifier().verify(TEST_URL, mock(SSLSession.class)));
        } catch (IOException e) {
            logger.warn("Test is not available - resource is not available: {}", e);
        } finally {
            if (httpConn != null) {
                httpConn.disconnect();
            }
        }
    }

    @Test
    public void testPrepFactory() throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException {
        assertNotNull(TrustModifier.prepFactory(mock(HttpsURLConnection.class)));
    }

}
