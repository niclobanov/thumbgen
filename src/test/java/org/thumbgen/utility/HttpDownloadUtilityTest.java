package org.thumbgen.utility;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.thumbgen.utility.HttpDownloadUtility;

public class HttpDownloadUtilityTest {

    private static final Logger logger = LoggerFactory.getLogger(HttpDownloadUtilityTest.class);
    
    public static final String TEST_IMAGE_FILE = "test-images/image_1.jpg";
    public static final String TEST_FILE_URL = "https://bitbucket.org/niclobanov/thumbgen/raw/00d6ec570338c98b27fb4f1e85bf077440189ea6/test-images/image_1.jpg";
    
    @Test
    public void testDownloadFile() {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        boolean result = false;
        try {
            result = HttpDownloadUtility.downloadFile(TEST_FILE_URL, outputStream);
            if (result) {
                assertTrue(Arrays.equals(Files.readAllBytes(new File(TEST_IMAGE_FILE).toPath()), outputStream.toByteArray()));
            } else {
                logger.warn("Test is not available - resource is not found");
            }
        } catch (IOException e) {
            logger.warn("Test is not available due to: {}", e);
        }
    }

}
