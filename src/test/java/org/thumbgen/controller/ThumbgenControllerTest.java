package org.thumbgen.controller;

import static org.hamcrest.CoreMatchers.containsString;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.InputStream;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.thumbgen.entity.Image;
import org.thumbgen.service.ThumbgenService;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;

@RunWith(SpringRunner.class)
@WebMvcTest
public class ThumbgenControllerTest {

    public static final byte[] ANY_DATA = "any_data".getBytes();
    
    public static final String IMAGE_FILE = "test-images/image_1.jpg";
    public static final String IMAGE_FILE_URL = "https://bitbucket.org/niclobanov/thumbgen/raw/00d6ec570338c98b27fb4f1e85bf077440189ea6/test-images/image_1.jpg";
    public static final String IMAGE_FILE_CONTENT = "base64_data_here";
    
    public static final String IMAGE_THUMBNAIL_MOCK_CONTENT_URLS = "base64_data_here_for_urls";
    public static final String IMAGE_THUMBNAIL_MOCK_CONTENT_FILES = "base64_data_here_for_files";
    public static final String IMAGE_THUMBNAIL_MOCK_CONTENT_JSON = "base64_data_here_for_json";
    
    public static final String TEST_URI = "/image/process";
    
    @Autowired
    private MockMvc mockMvc;
    
    @MockBean
    private ThumbgenService thumbgenService;
    
    @Test
    public void testProcessUrlData() throws Exception {
        when(thumbgenService.generateThubnail(any(InputStream.class)))
            .thenReturn(IMAGE_THUMBNAIL_MOCK_CONTENT_URLS);
        
        MultiValueMap<String, String> urls = new LinkedMultiValueMap<>();
        urls.add("url", IMAGE_FILE_URL);
        
        mockMvc.perform(post(TEST_URI)
            .params(urls))
            .andExpect(status().isOk())
        .andExpect(content().string(containsString(IMAGE_THUMBNAIL_MOCK_CONTENT_URLS)));
    }
    
    @Test
    public void testProcessMultipartFormData() throws Exception {
        when(thumbgenService.generateThubnail(any(InputStream.class)))
            .thenReturn(IMAGE_THUMBNAIL_MOCK_CONTENT_FILES);
        
        MockMultipartFile file = new MockMultipartFile("file", IMAGE_FILE, null, ANY_DATA);
        
        this.mockMvc.perform(
                    MockMvcRequestBuilders.multipart(TEST_URI)
                    .file(file).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString(IMAGE_THUMBNAIL_MOCK_CONTENT_FILES)));
    }
    
    @Test
    public void testProcessBase64Data() throws Exception {
        when(thumbgenService.generateThubnail(any(String.class)))
            .thenReturn(IMAGE_THUMBNAIL_MOCK_CONTENT_JSON);
        
        Image image = new Image(IMAGE_FILE, IMAGE_FILE_CONTENT);
        Image[] images = {image};
        
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson = ow.writeValueAsString(images);
        
        mockMvc.perform(post(TEST_URI).contentType(MediaType.APPLICATION_JSON_UTF8)
            .content(requestJson))
            .andExpect(status().isOk())
        .andExpect(content().string(containsString(IMAGE_THUMBNAIL_MOCK_CONTENT_JSON)));
    }
}
