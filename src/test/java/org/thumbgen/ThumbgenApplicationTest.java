package org.thumbgen;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.thumbgen.controller.ThumbgenController;
import org.thumbgen.entity.Image;
import org.thumbgen.entity.Thumbnail;
import org.thumbgen.service.ThumbgenService;
import org.thumbgen.utility.HttpDownloadUtility;
import org.thumbgen.utility.UtilBase64Image;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assume.assumeTrue;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment=WebEnvironment.DEFINED_PORT)
public class ThumbgenApplicationTest {

    private static final Logger logger = LoggerFactory.getLogger(ThumbgenApplicationTest.class);
    
    public static String SERVER_TEST_URL = "http://localhost:8081/image/process/";
    
    public static final String TEST_IMAGE_FILE_1 = "test-images/image_1.jpg";
    public static final String TEST_IMAGE_FILE_2 = "test-images/image_2.jpg";
    public static final String TEST_IMAGE_FILE_3 = "test-images/image_3.jpg";
    public static final String TEST_IMAGE_FILE_4 = "test-images/image_4.jpg";
    
    public static final String TEST_IMAGE_URL_1 = "https://bitbucket.org/niclobanov/thumbgen/raw/00d6ec570338c98b27fb4f1e85bf077440189ea6/test-images/image_1.jpg";
    public static final String TEST_IMAGE_URL_2 = "https://bitbucket.org/niclobanov/thumbgen/raw/00d6ec570338c98b27fb4f1e85bf077440189ea6/test-images/image_2.jpg";
    public static final String TEST_IMAGE_URL_3 = "https://bitbucket.org/niclobanov/thumbgen/raw/00d6ec570338c98b27fb4f1e85bf077440189ea6/test-images/image_3.jpg";
    public static final String TEST_IMAGE_URL_4 = "https://bitbucket.org/niclobanov/thumbgen/raw/00d6ec570338c98b27fb4f1e85bf077440189ea6/test-images/image_4.jpg";
    
    public static final String[] TEST_IMAGES_URLS = {TEST_IMAGE_URL_1, TEST_IMAGE_URL_2, TEST_IMAGE_URL_3, TEST_IMAGE_URL_4};
    
    static boolean checkImageUrlTestsAreAvailableOnce = false;
    static boolean imageUrlTestsAreAvailable = true;
    
    @Autowired
    private TestRestTemplate restTemplate;
    
    @Autowired
    private ThumbgenController thumbgenController;
    
    @Autowired
    private ThumbgenService thumbgenService;
    
    @Before
    public void remoteImagesAccessCheck() {
        if (!checkImageUrlTestsAreAvailableOnce) {
            checkImageUrlTestsAreAvailableOnce = true;
            logger.info("Checking remote images availabilty...");
            for (String url : TEST_IMAGES_URLS) {
                ByteArrayOutputStream urlImageStream = new ByteArrayOutputStream();
                try {
                    HttpDownloadUtility.downloadFile(url, urlImageStream);
                    imageUrlTestsAreAvailable = urlImageStream.toByteArray().length != 0;
                    if (!imageUrlTestsAreAvailable) {
                        logger.warn("One of remote images {} is not exist. Remote images tests would be ignored", url);
                        break;
                    }
                } catch (IOException e) {
                    logger.warn("Remote images tests would be ignored due to next exception: {}", e);
                    imageUrlTestsAreAvailable = false;
                    break;
                }
            }
            logger.info("Remote images availability: {}", imageUrlTestsAreAvailable);
        }
    }
    
    @Test
    public void contextLoads() {
        assertThat(thumbgenController).isNotNull();
        assertThat(thumbgenService).isNotNull();
    }
    
    @Test
    public void checkProcessSingleImageFile() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        
        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        body.add("file", new FileSystemResource(new File(TEST_IMAGE_FILE_1)));
        HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(body, headers);
        ResponseEntity<List<Thumbnail>> response = restTemplate.exchange(SERVER_TEST_URL, HttpMethod.POST, requestEntity, new ParameterizedTypeReference<List<Thumbnail>>(){});
        
        assertThat(response.getBody().size() == 1).isTrue();
        
        List<Thumbnail> thumbnails = response.getBody();
        for (Thumbnail thumb : thumbnails) {
            assertTrue(thumb.isSuccess());
        }
    }
    
    @Test
    public void checkProcessMulitpleImageFile() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        body.add("file", new FileSystemResource(new File(TEST_IMAGE_FILE_2)));
        body.add("file", new FileSystemResource(new File(TEST_IMAGE_FILE_3)));
        body.add("file", new FileSystemResource(new File(TEST_IMAGE_FILE_4)));
        HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(body, headers);
        
        ResponseEntity<List<Thumbnail>> response = 
                restTemplate.exchange(SERVER_TEST_URL, HttpMethod.POST, requestEntity, new ParameterizedTypeReference<List<Thumbnail>>(){});
        
        assertThat(response.getBody().size() == 3).isTrue();
        
        List<Thumbnail> thumbnails = response.getBody();
        for (Thumbnail thumb : thumbnails) {
            assertTrue(thumb.isSuccess());
        }
    }
    
    @Test
    public void checkProcessSingleBase64Image() {
        Image image1 = new Image(TEST_IMAGE_FILE_1, UtilBase64Image.encoder(TEST_IMAGE_FILE_1));
        Image[] images = {image1};
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<Image[]> requestEntity = new HttpEntity<>(images, headers);
        
        ResponseEntity<List<Thumbnail>> response = 
                restTemplate.exchange(SERVER_TEST_URL, HttpMethod.POST, requestEntity, new ParameterizedTypeReference<List<Thumbnail>>(){});
        
        assertThat(response.getBody().size() == 1).isTrue();
        
        List<Thumbnail> thumbnails = response.getBody();
        for (Thumbnail thumb : thumbnails) {
            assertTrue(thumb.isSuccess());
        }
    }
    
    @Test
    public void checkProcessMultipleBase64Image() {
        Image image2 = new Image(TEST_IMAGE_FILE_2, UtilBase64Image.encoder(TEST_IMAGE_FILE_2));
        Image image3 = new Image(TEST_IMAGE_FILE_3, UtilBase64Image.encoder(TEST_IMAGE_FILE_3));
        Image image4 = new Image(TEST_IMAGE_FILE_4, UtilBase64Image.encoder(TEST_IMAGE_FILE_4));
        
        Image[] images = {image2, image3, image4};
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<Image[]> requestEntity = new HttpEntity<>(images, headers);
        
        ResponseEntity<List<Thumbnail>> response = 
                restTemplate.exchange(SERVER_TEST_URL, HttpMethod.POST, requestEntity, new ParameterizedTypeReference<List<Thumbnail>>(){});
        assertThat(response.getBody().size() == 3).isTrue();
        
        List<Thumbnail> thumbnails = response.getBody();
        for (Thumbnail thumb : thumbnails) {
            assertTrue(thumb.isSuccess());
        }
    }
    
    @Test
    public void checkProcessSingleImageUrl() {
        assumeTrue(imageUrlTestsAreAvailable);
        
        HttpHeaders headers = new HttpHeaders();
        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        body.add("url", TEST_IMAGE_URL_1);
        HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(body, headers);
        
        ResponseEntity<List<Thumbnail>> response = 
                restTemplate.exchange(SERVER_TEST_URL, HttpMethod.POST, requestEntity, new ParameterizedTypeReference<List<Thumbnail>>(){});
        
        assertThat(response.getBody().size() == 1).isTrue();
        
        List<Thumbnail> thumbnails = response.getBody();
        for (Thumbnail thumb : thumbnails) {
            assertTrue(thumb.isSuccess());
        }
    }
    
    @Test
    public void checkProcessMulitpleImageUrl() {
        assumeTrue(imageUrlTestsAreAvailable);
        
        HttpHeaders headers = new HttpHeaders();
        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        body.add("url", TEST_IMAGE_URL_2);
        body.add("url", TEST_IMAGE_URL_3);
        body.add("url", TEST_IMAGE_URL_4);
        
        HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(body, headers);
        ResponseEntity<List<Thumbnail>> response = 
                restTemplate.exchange(SERVER_TEST_URL, HttpMethod.POST, requestEntity, new ParameterizedTypeReference<List<Thumbnail>>(){});
        
        assertThat(response.getBody().size() == 3).isTrue();
        
        List<Thumbnail> thumbnails = response.getBody();
        for (Thumbnail thumb : thumbnails) {
            assertTrue(thumb.isSuccess());
        }
    }
}
