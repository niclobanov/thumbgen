package org.thumbgen.entity;

import static org.junit.Assert.*;

import org.junit.Test;
import org.thumbgen.entity.Thumbnail;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;

public class ThumbnailTest {

    public static final String LS = System.lineSeparator();
    public static final String JSON_THUMBNAIL_REPRESENTATION = String.format("{%s  \"item\" : \"test.jpg\",%s  \"value\" : \"base64_content_here\",%s  \"success\" : true%s}", 
                LS, LS, LS, LS);
    // public static final String JSON_THUMBNAIL_REPRESENTATION = "{\r\n  \"item\" : \"test.jpg\",\r\n  \"value\" : \"base64_content_here\",\r\n  \"success\" : true\r\n}";
    
    @Test
    public void testThumbnailAsJson() throws JsonProcessingException {
        Thumbnail thumbnail = new Thumbnail("test.jpg", "base64_content_here", true);
        
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String jsonImage = ow.writeValueAsString(thumbnail);
        
        assertEquals(JSON_THUMBNAIL_REPRESENTATION, jsonImage);
    }

}
