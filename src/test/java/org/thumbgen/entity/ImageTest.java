package org.thumbgen.entity;

import static org.junit.Assert.*;

import org.junit.Test;
import org.thumbgen.entity.Image;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;

public class ImageTest {

    public static final String LS = System.lineSeparator();
    public static final String JSON_IMAGE_REPRESENTATION = String.format("{%s  \"fileName\" : \"test.jpg\",%s  \"value\" : \"base64_contet_here\"%s}", 
            LS, LS, LS);
    // public static final String JSON_IMAGE_REPRESENTATION = "{\r\n  \"fileName\" : \"test.jpg\",\r\n  \"value\" : \"base64_contet_here\"\r\n}";
    
    @Test
    public void testImageAsJson() throws JsonProcessingException {
        Image image = new Image("test.jpg", "base64_contet_here");
        
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String jsonImage = ow.writeValueAsString(image);
        
        assertEquals(JSON_IMAGE_REPRESENTATION, jsonImage);
    }

}
